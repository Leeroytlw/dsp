import matplotlib.pyplot as plt
import numpy as np
from scipy.io import wavfile

sample_rate, data = wavfile.read('../third_lesson/recorded.wav')

data_count = data.shape[0]

duration = data_count / sample_rate
t = np.linspace(0, duration, data_count)

plt.plot(t, data)
plt.xlim([0, duration])
plt.yticks(np.linspace(np.floor(np.min(data)), np.ceil(np.max(data)), 9))
plt.grid(True)
plt.tight_layout()
plt.show()

data = data.flatten()
d = np.gradient(data)
s_d = np.gradient(d)
where = np.where(s_d == 0)
print(where[0].size)
plt.plot(s_d)
plt.show()

t_l = 0.005
t_u = 0.0195

# condition = np.where(d >= t_l)
# first = condition[0][0]
# last = condition[0][-1]

decoded = []

for i in range(0, where[0].size, int(where[0].size / duration)):
    # print(where[0][i], d[where[0][i]])
    if t_l <= d[where[0][i]] < t_u:
        decoded.append(0)
    elif d[where[0][i]] >= t_u:
        decoded.append(1)

print(decoded)
