import matplotlib.pyplot as plt
import numpy as np
from scipy.io import wavfile


def get_fsk_signal(mod_signal, zero_frequency, one_frequency):
    carrier_period = mod_signal.size
    mod_frequencies = np.zeros(carrier_period)
    mod_frequencies[mod_signal == 0] = zero_frequency
    mod_frequencies[mod_signal == 1] = one_frequency
    return np.sin(mod_frequencies * 2 * np.pi * np.linspace(0, 1, carrier_period))


def main():
    bits_count = 25
    width = 4000

    mod_sequence = np.random.randint(0, 2, bits_count)

    mod_signal = np.repeat(mod_sequence, width)
    psk_signal = get_fsk_signal(mod_signal, 4000, 10000)

    plt.figure(figsize=(14, 5))
    plt.grid(True)
    plt.plot(psk_signal)
    plt.plot(mod_signal, '--')
    plt.show()

    wavfile.write("msg.wav", width, psk_signal)

    np.savetxt('source_sequence.txt', mod_sequence, fmt="%i", newline=' ')


main()
